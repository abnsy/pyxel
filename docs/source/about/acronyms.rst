.. _acronyms:

List of Acronyms
=================

=======          ==========
Acronym          Definition
=======          ==========
CCD              Charge-Coupled Device
CMOS             Complementary Metal Oxide Semiconductor
MCT              Mercury Cadmium Telluride (HgCdTe)

CDM              Charge Distortion Model

CTI              Charge Transfer Inefficiency
SNR              Signal-to-Noise Ratio
QE               Quantum Efficiency
PNRU             Photo-Response Non-Uniformity
FPN              Fixed-pattern noise
ADC              Analog-Digital Converter
PSF              Point Spread Function
ACN              Alternating Column Noise
RTG              Random Telegraph Noise
=======          ==========
