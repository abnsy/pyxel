=====================
Citation instructions
=====================

.. _citation:

Cite the library
================

Pyxel repository currently does not have a DOI of its own yet.

Citing the paper
================

If you are using Pyxel as part of your research, teaching, or other activities,
we would be grateful if you could start the repository and/or cite our work.

For citation purposes, you can use the following BibTex entries:

Pyxel: the collaborative detection simulation framework (2020)
--------------------------------------------------------------

From SPIE Astronomical Telescopes + Instrumentation 2020::

    @inproceedings{10.1117/12.2561731,
        author = {Thibaut Prod'homme and Frédéric Lemmel and Matej Arko and Benoit Serra and Elizabeth George and Enrico Biancalani and Hans Smit and David Lucsanyi},
        title = {{Pyxel: the collaborative detection simulation framework}},
        volume = {11454},
        booktitle = {X-Ray, Optical, and Infrared Detectors for Astronomy IX},
        editor = {Andrew D. Holland and James Beletic},
        organization = {International Society for Optics and Photonics},
        publisher = {SPIE},
        pages = {26 -- 35},
        keywords = {Detectors, Modelling, MCT, Instrumentation, Simulation, MKID, Persistence, data analysis},
        year = {2020},
        doi = {10.1117/12.2561731},
        URL = {https://doi.org/10.1117/12.2561731}
    }


Pyxel: a novel and multi-purpose Python-based framework for imaging detector simulation (2018)
----------------------------------------------------------------------------------------------

From SPIE Astronomical Telescopes + Instrumentation 2018::

    @inproceedings{10.1117/12.2314047,
        author = {David Lucsanyi and Thibaut Prod'homme and Hans Smit and Frederic Lemmel and Pierre-Elie Crouzet and Peter Verhoeve and Brian Shortt},
        title = {{Pyxel: a novel and multi-purpose Python-based framework for imaging detector simulation}},
        volume = {10709},
        booktitle = {High Energy, Optical, and Infrared Detectors for Astronomy VIII},
        editor = {Andrew D. Holland and James Beletic},
        organization = {International Society for Optics and Photonics},
        publisher = {SPIE},
        pages = {325 -- 333},
        keywords = {detector simulation framework, python, open-source, pyxel, cti charge transfer inefficiency, CCD, cmos imager, Software},
        year = {2018},
        doi = {10.1117/12.2314047},
        URL = {https://doi.org/10.1117/12.2314047}
    }

