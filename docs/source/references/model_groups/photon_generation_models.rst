.. _photon_generation:

Photon Generation models
========================


.. currentmodule:: pyxel.models.photon_generation
.. automodule:: pyxel.models.photon_generation


Loading image
-------------

.. autofunction:: load_image

Simple illumination
-------------------

.. autofunction:: illumination

Stripe pattern
--------------

.. autofunction:: stripe_pattern

Shot noise
----------

.. autofunction:: shot_noise
