.. _charge_collection:

Charge Collection models
========================

.. currentmodule:: pyxel.models.charge_collection
.. automodule:: pyxel.models.charge_collection


Simple charge collection
------------------------

.. autofunction:: simple_collection

Simple full well
----------------

.. autofunction:: simple_full_well

Fix pattern noise
-----------------

.. autofunction:: fix_pattern_noise
   :noindex:

Inter-pixel capacitance
-----------------------

.. autofunction:: simple_ipc

Persistence
-----------

.. autofunction:: simple_persistence
.. autofunction:: current_persistence
