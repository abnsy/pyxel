.. _readout_electronics:

Readout Electronics models
==========================

.. currentmodule:: pyxel.models.readout_electronics
.. automodule:: pyxel.models.readout_electronics


Simple digitization
-------------------

.. autofunction:: simple_digitization

Simple amplification
--------------------

.. autofunction:: simple_amplifier

DC crosstalk
--------------

.. autofunction:: dc_crosstalk

AC crosstalk
--------------

.. autofunction:: ac_crosstalk
