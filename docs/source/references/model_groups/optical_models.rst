.. _optical:

Optical models
==============

.. currentmodule:: pyxel.models.optics
.. automodule:: pyxel.models.optics


Physical Optics Propagation in PYthon (POPPY)
---------------------------------------------

.. autofunction:: optical_psf


Simple optical alignment
------------------------

.. autofunction:: alignment
