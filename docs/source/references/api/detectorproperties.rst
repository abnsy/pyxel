.. _detectorproperties_api:

===================
Detector properties
===================
.. currentmodule:: pyxel.detectors

Material
--------
.. autoclass:: Material
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

Environment
-----------
.. autoclass:: Environment
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

Characteristics
---------------
.. autoclass:: Characteristics
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

Geometry
--------
.. autoclass:: Geometry
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CCD specific
------------
CCD specific classes.

CCDCharacteristics
==================
.. autoclass:: CCDCharacteristics
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CCDGeometry
===========
.. autoclass:: CCDGeometry
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CMOS specific
-------------
CMOS specific classes.

CMOSCharacteristics
===================
.. autoclass:: CMOSCharacteristics
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CMOSGeoemtry
============
.. autoclass:: CMOSGeometry
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:
