.. _detectors_api:

=========
Detectors
=========

.. currentmodule:: pyxel.detectors

Detector
--------

.. autoclass:: Detector
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CCD
---
.. autoclass:: CCD
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CMOS
----

.. autoclass:: CMOS
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:
