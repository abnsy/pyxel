.. _howtos:

========
Overview
========

**Step-by-step guides**. Covers key tasks and operations and common problems.
These how-to guides are intended as recipes to solve common problems/tasks using Pyxel.

They are composed of the following **goal-oriented** series of steps:

.. note::

    We assume that the user has already some basic knowledge and understanding of Pyxel.

    If you are a beginner, you should have a look at the tutorials.

    A how-to guide is an answer to a question that only a user with some
    experience could even formulate and it offers a get-it-done information.

* Working in the local development environment
* :doc:`new_model`
