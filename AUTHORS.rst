Authors
============

Developers:

* David Lucsanyi
* `Matej Arko <matej.arko@esa.int>`_
* `Thibaut Prod'homme <thibaut.prodhomme@esa.int>`_
* `Hans Smit <hans.smit@esa.int>`_
* `Frederic Lemmel <frederic.lemmel@esa.int>`_
* `Benoit Serra <benoit.serra@eso.org>`_

Contributors:

* Elizabeth George
* Pierre-Elie Crouzet
* Peter Verhoeve
* Brian Shortt
